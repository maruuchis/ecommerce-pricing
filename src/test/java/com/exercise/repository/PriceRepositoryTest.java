package com.exercise.repository;

import com.exercise.model.Price;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@DataJpaTest
public class PriceRepositoryTest {

    @Autowired
    private PriceRepository priceRepository;

    @Test
    public void findByProductIdAndBrandIdAndApplicationDateOneResultTest() {

        //Arrange
        Long productId = 35455L;
        Long brandId = 1L;
        LocalDateTime dateTime = LocalDateTime.parse("2020-06-14T01:00:00");

        //Act
        List<Price> prices = priceRepository.findByProductIdAndBrandIdAndDateTime(productId, brandId, dateTime);

        //Assert
        Assertions.assertEquals(1, prices.size());
        Price price = prices.get(0);
        Assertions.assertEquals(1L, price.getPriceId());

    }

    @Test
    public void findByProductIdAndBrandIdAndApplicationDateWithoutResultTest() {

        //Arrange
        Long productId = 35455L;
        Long brandId = 1L;
        LocalDateTime dateTime = LocalDateTime.parse("2021-06-15T11:00:00");

        //Act
        List<Price> prices = priceRepository.findByProductIdAndBrandIdAndDateTime(productId, brandId, dateTime);

        //Assert
        Assertions.assertEquals(0, prices.size());

    }

    @Test
    public void findByProductIdAndBrandIdAndApplicationDateWithTwoResultsTest() {

        //Arrange
        Long productId = 35455L;
        Long brandId = 1L;
        LocalDateTime dateTime = LocalDateTime.parse("2020-06-15T11:00:00");

        //Act
        List<Price> prices = priceRepository.findByProductIdAndBrandIdAndDateTime(productId, brandId, dateTime);

        //Assert
        Assertions.assertEquals(2, prices.size());

        List<Price> sortedPrices = prices.stream().sorted(Comparator.comparingLong(Price::getPriceId))
                .collect(Collectors.toList());
        Price price1 = sortedPrices.get(0);
        Assertions.assertEquals(1L, price1.getPriceId());
        Price price2 = sortedPrices.get(1);
        Assertions.assertEquals(3L, price2.getPriceId());

    }
}
