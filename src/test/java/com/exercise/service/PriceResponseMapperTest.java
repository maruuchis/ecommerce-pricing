package com.exercise.service;

import com.exercise.dto.PriceResponseDto;
import com.exercise.model.Price;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

@ExtendWith(MockitoExtension.class)
public class PriceResponseMapperTest {

    @InjectMocks
    private PriceResponseMapper responseMapper;

    @Test
    public void toPriceResponseDtoTest(){

        //Arrange
        Long priceId = 2L;
        Long brandId = 1L;
        Long productId = 2L;
        LocalDateTime startDate = LocalDateTime.parse("2020-06-14T11:00:00");
        LocalDateTime endDate = LocalDateTime.parse("2020-06-15T12:00:00");
        Double priceValue = 36.2;
        String currency = "EUR";
        int priority = 1;

        Price price = new Price(priceId, brandId, productId, startDate, endDate, priority, priceValue, currency);

        //Act
        PriceResponseDto priceResponseDto = responseMapper.toPriceResponseDto(price);

        //Assert
        Assertions.assertEquals(priceId, priceResponseDto.getPriceId());
        Assertions.assertEquals(brandId, priceResponseDto.getBrandId());
        Assertions.assertEquals(productId, priceResponseDto.getProductId());
        Assertions.assertEquals(startDate.toString(), priceResponseDto.getStartDate());
        Assertions.assertEquals(endDate.toString(), priceResponseDto.getEndDate());
        Assertions.assertEquals(priceValue, priceResponseDto.getPrice());
        Assertions.assertEquals(currency, priceResponseDto.getCurrency());

    }

}
