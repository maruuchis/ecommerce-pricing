package com.exercise.service;

import com.exercise.dto.PriceResponseDto;
import com.exercise.model.Price;
import com.exercise.repository.PriceRepository;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class PriceServiceImplTest {

    @Mock
    private PriceRepository priceRepository;

    @Mock
    private PriceResponseMapper priceResponseMapper;

    @InjectMocks
    private PriceServiceImpl priceServiceImpl;

    @Test
    public void getProductPriceWithoutPriceTest(){

        //Arrange
        Long brandId = 1L;
        LocalDateTime date = LocalDateTime.parse("2020-06-15T11:00:00");
        Long productId = 2L;

        List<Price> prices = Lists.newArrayList();

        Mockito.when(priceRepository.findByProductIdAndBrandIdAndDateTime(productId, brandId, date))
                .thenReturn(prices);

        //Act
        Optional<PriceResponseDto> productPrice = priceServiceImpl.getProductPriceForDateTime(productId, brandId, date);

        //Assert
        Assertions.assertTrue(productPrice.isEmpty());
    }

    @Test
    public void getProductPriceWithOnePriceTest(){

        //Arrange
        Long brandId = 1L;
        LocalDateTime date = LocalDateTime.parse("2020-06-15T11:00:00");

        Long priceId = 2L;
        LocalDateTime startDate = LocalDateTime.parse("2020-06-14T11:00:00");
        LocalDateTime endDate = LocalDateTime.parse("2020-06-15T12:00:00");
        Double priceValue = 36.2;
        String currency = "EUR";
        Long productId = 2L;
        int priority = 1;

        Price price = new Price(priceId, brandId, productId, startDate, endDate, priority, priceValue, currency);
        List<Price> prices = Lists.newArrayList(price);

        Mockito.when(priceRepository.findByProductIdAndBrandIdAndDateTime(productId, brandId, date))
                .thenReturn(prices);

        PriceResponseDto responseDto = PriceResponseDto.PriceResponseDtoBuilder.aPriceResponseDto().build();
        Mockito.when(priceResponseMapper.toPriceResponseDto(price))
                .thenReturn(responseDto);

        //Act
        Optional<PriceResponseDto> priceResponseDto = priceServiceImpl.getProductPriceForDateTime(productId, brandId, date);

        //Assert
        Assertions.assertFalse(priceResponseDto.isEmpty());
        Assertions.assertEquals(responseDto, priceResponseDto.get());
    }

    @Test
    public void getProductPriceWithTwoPricesTest(){

        //Arrange
        Long brandId = 1L;
        LocalDateTime date = LocalDateTime.parse("2020-06-15T11:00:00");
        String currency = "EUR";
        Long productId = 2L;

        Long priceId = 2L;
        LocalDateTime startDate = LocalDateTime.parse("2020-06-14T11:00:00");
        LocalDateTime endDate = LocalDateTime.parse("2020-06-15T12:00:00");
        Double priceValue = 36.2;
        int priority = 2;
        Price price1 = new Price(priceId, brandId, productId, startDate, endDate, priority, priceValue, currency);

        Price price2 = new Price(2L, brandId, productId, startDate, endDate, 1, 40.36, currency);
        List<Price> prices = Lists.newArrayList(price1, price2);

        Mockito.when(priceRepository.findByProductIdAndBrandIdAndDateTime(productId, brandId, date))
                .thenReturn(prices);

        PriceResponseDto responseDto = PriceResponseDto.PriceResponseDtoBuilder.aPriceResponseDto().build();
        Mockito.when(priceResponseMapper.toPriceResponseDto(price1))
                .thenReturn(responseDto);

        //Act
        Optional<PriceResponseDto> priceResponseDto = priceServiceImpl.getProductPriceForDateTime(productId, brandId, date);

        //Assert
        Assertions.assertFalse(priceResponseDto.isEmpty());
        Assertions.assertEquals(responseDto, priceResponseDto.get());
    }

}
