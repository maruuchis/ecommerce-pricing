package com.exercise.integration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class IntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetProductPriceCase1Test() throws Exception {

        String dateTime = "2020-06-14T10:00:00";
        String brandId = "1";
        String productId = "35455";

        MockHttpServletResponse response = mockMvc.perform(get("/prices?")
                        .param("dateTime", dateTime)
                        .param("brandId", brandId)
                        .param("productId", productId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        String jsonExpected = "{\"productId\":35455,\"price\":35.5,\"currency\":\"EUR\",\"brandId\":1,\"priceId\":1,\"startDate\":\"2020-06-14T00:00\",\"endDate\":\"2020-12-31T23:59:59\"}";
        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
        Assertions.assertEquals(jsonExpected, response.getContentAsString());

    }

    @Test
    public void testGetProductPriceCase2Test() throws Exception {

        String dateTime = "2020-06-14T16:00:00";
        String brandId = "1";
        String productId = "35455";

        MockHttpServletResponse response = mockMvc.perform(get("/prices?")
                .param("dateTime", dateTime)
                .param("brandId", brandId)
                .param("productId", productId)
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        String jsonExpected = "{\"productId\":35455,\"price\":25.45,\"currency\":\"EUR\",\"brandId\":1,\"priceId\":2,\"startDate\":\"2020-06-14T15:00\",\"endDate\":\"2020-06-14T18:30\"}";
        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
        Assertions.assertEquals(jsonExpected, response.getContentAsString());

    }

    @Test
    public void testGetProductPriceCase3Test() throws Exception {

        String dateTime = "2020-06-14T21:00:00";
        String brandId = "1";
        String productId = "35455";

        MockHttpServletResponse response = mockMvc.perform(get("/prices?")
                .param("dateTime", dateTime)
                .param("brandId", brandId)
                .param("productId", productId)
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        String jsonExpected = "{\"productId\":35455,\"price\":35.5,\"currency\":\"EUR\",\"brandId\":1,\"priceId\":1,\"startDate\":\"2020-06-14T00:00\",\"endDate\":\"2020-12-31T23:59:59\"}";
        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
        Assertions.assertEquals(jsonExpected, response.getContentAsString());

    }

    @Test
    public void testGetProductPriceCase4Test() throws Exception {

        String dateTime = "2020-06-15T10:00:00";
        String brandId = "1";
        String productId = "35455";

        MockHttpServletResponse response = mockMvc.perform(get("/prices?")
                .param("dateTime", dateTime)
                .param("brandId", brandId)
                .param("productId", productId)
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        String jsonExpected = "{\"productId\":35455,\"price\":30.5,\"currency\":\"EUR\",\"brandId\":1,\"priceId\":3,\"startDate\":\"2020-06-15T00:00\",\"endDate\":\"2020-06-15T11:00\"}";
        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
        Assertions.assertEquals(jsonExpected, response.getContentAsString());

    }

    @Test
    public void testGetProductPriceCase5Test() throws Exception {

        String dateTime = "2020-06-16T21:00:00";
        String brandId = "1";
        String productId = "35455";

        MockHttpServletResponse response = mockMvc.perform(get("/prices?")
                .param("dateTime", dateTime)
                .param("brandId", brandId)
                .param("productId", productId)
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        String jsonExpected = "{\"productId\":35455,\"price\":38.95,\"currency\":\"EUR\",\"brandId\":1,\"priceId\":4,\"startDate\":\"2020-06-15T16:00\",\"endDate\":\"2020-12-31T23:59:59\"}";
        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
        Assertions.assertEquals(jsonExpected, response.getContentAsString());

    }

    @Test
    public void testGetProductPriceNotFoundTest() throws Exception {

        String dateTime = "2021-06-16T21:00:50";
        String brandId = "1";
        String productId = "35455";

        MockHttpServletResponse response = mockMvc.perform(get("/prices?")
                .param("dateTime", dateTime)
                .param("brandId", brandId)
                .param("productId", productId)
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatus());
        String expectedMessage = "Price not found for productId: 35455, brandId: 1, dateTime: 2021-06-16T21:00:50";
        Assertions.assertEquals(expectedMessage, response.getContentAsString());

    }

    @Test
    public void testGetProductPriceWithoutBrandIdTest() throws Exception {

        String dateTime = "2021-06-16T21:00:50";
        String productId = "35455";

        MockHttpServletResponse response = mockMvc.perform(get("/prices?")
                .param("dateTime", dateTime)
                .param("productId", productId)
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        String expectedMessage = "Required request parameter 'brandId' for method parameter type Long is not present";
        Assertions.assertEquals(expectedMessage, response.getErrorMessage());

    }
}
