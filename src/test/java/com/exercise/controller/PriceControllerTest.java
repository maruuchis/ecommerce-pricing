package com.exercise.controller;

import com.exercise.dto.PriceResponseDto;
import com.exercise.exception.PriceNotFoundException;
import com.exercise.service.PriceService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@ExtendWith(MockitoExtension.class)
public class PriceControllerTest {

    private MockMvc mockMvc;

    @Mock
    private PriceService priceService;

    @InjectMocks
    private PriceController priceController;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(priceController)
                .build();
    }

    @Test
    public void getProductPriceTestWithNotFoundException() {

        //Arrange
        LocalDateTime dateTime = LocalDateTime.parse("2020-06-15T11:00:00");
        Long brandId = 1L;
        Long productId = 2524L;

        Mockito.when(priceService.getProductPriceForDateTime(productId, brandId, dateTime))
                .thenReturn(Optional.empty());

        //Act
        Exception expectedException = Assertions.assertThrows(NestedServletException.class, () ->
            mockMvc.perform(get("/prices?")
                .param("dateTime", dateTime.toString())
                .param("brandId", String.valueOf(brandId))
                .param("productId", String.valueOf(productId))
                .contentType(MediaType.APPLICATION_JSON)));

        //Assert
        Throwable cause = expectedException.getCause();
        Assertions.assertTrue(cause instanceof PriceNotFoundException);
        String exceptedMessage = "Price not found for productId: 2524, brandId: 1, dateTime: 2020-06-15T11:00";
        Assertions.assertEquals(exceptedMessage , cause.getMessage());

    }

    @Test
    public void getProductPriceTestWithSuccess() throws Exception {

        //Arrange
        LocalDateTime dateTime = LocalDateTime.parse("2020-06-15T11:00:00");
        Long brandId = 1L;
        Long productId = 2524L;

        PriceResponseDto priceResponseDto = PriceResponseDto.PriceResponseDtoBuilder.aPriceResponseDto().build();
        Mockito.when(priceService.getProductPriceForDateTime(productId, brandId, dateTime))
                .thenReturn(Optional.of(priceResponseDto));

        //Act
        MockHttpServletResponse response = mockMvc.perform(get("/prices?")
                .param("dateTime", dateTime.toString())
                .param("brandId", String.valueOf(brandId))
                .param("productId", String.valueOf(productId))
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        //Assert
        String jsonExpected = "{\"productId\":null,\"price\":null,\"currency\":null,\"brandId\":null,\"priceId\":null,\"startDate\":null,\"endDate\":null}";
        assertEquals(HttpStatus.OK.value(), response.getStatus());
        assertEquals(jsonExpected, response.getContentAsString());

    }

    @Test
    public void getProductPriceTestWithBadRequestMissingBrandId() throws Exception {

        //Arrange
        LocalDateTime dateTime = LocalDateTime.parse("2020-06-15T11:00:00");
        Long productId = 2524L;

        //Act
        MockHttpServletResponse response = mockMvc.perform(get("/prices?")
                .param("dateTime", dateTime.toString())
                .param("productId", String.valueOf(productId))
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        //Assert
        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        String expectedMessage = "Required request parameter 'brandId' for method parameter type Long is not present";
        assertEquals(expectedMessage, response.getErrorMessage());

    }

    @Test
    public void getProductPriceTestWithBadRequestMissingProductId() throws Exception {

        //Arrange
        LocalDateTime dateTime = LocalDateTime.parse("2020-06-15T11:00:00");
        Long brandId = 1L;


        //Act
        MockHttpServletResponse response = mockMvc.perform(get("/prices?")
                .param("dateTime", dateTime.toString())
                .param("brandId", String.valueOf(brandId))
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        //Assert
        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        String expectedMessasge = "Required request parameter 'productId' for method parameter type Long is not present";
        assertEquals(expectedMessasge, response.getErrorMessage());

    }

    @Test
    public void getProductPriceTestWithBadRequestMissingDateTime() throws Exception {

        //Arrange
        Long brandId = 1L;
        Long productId = 2524L;

        //Act
        MockHttpServletResponse response = mockMvc.perform(get("/prices?")
                .param("brandId", String.valueOf(brandId))
                .param("productId", String.valueOf(productId))
                .contentType(MediaType.APPLICATION_JSON)).andReturn().getResponse();

        //Assert
        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
        String expectedMessage = "Required request parameter 'dateTime' for method parameter type LocalDateTime is not present";
        assertEquals(expectedMessage, response.getErrorMessage());

    }
}