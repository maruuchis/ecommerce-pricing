package com.exercise.dto;


public class PriceResponseDto {

    private Long productId;
    private Double price;
    private String currency;

    private Long brandId;
    private Long priceId;
    private String startDate;
    private String endDate;

    public PriceResponseDto(Long productId, Double price, String currency, Long brandId,
                            Long priceId, String startDate, String endDate) {
        this.productId = productId;
        this.price = price;
        this.currency = currency;
        this.brandId = brandId;
        this.priceId = priceId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Long getProductId() {
        return productId;
    }

    public Double getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }

    public Long getBrandId() {
        return brandId;
    }

    public Long getPriceId() {
        return priceId;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }


    public static final class PriceResponseDtoBuilder {
        private Long productId;
        private Double price;
        private String currency;
        private Long brandId;
        private Long priceId;
        private String startDate;
        private String endDate;

        private PriceResponseDtoBuilder() {
        }

        public static PriceResponseDtoBuilder aPriceResponseDto() {
            return new PriceResponseDtoBuilder();
        }

        public PriceResponseDtoBuilder withProductId(Long productId) {
            this.productId = productId;
            return this;
        }

        public PriceResponseDtoBuilder withPrice(Double price) {
            this.price = price;
            return this;
        }

        public PriceResponseDtoBuilder withCurrency(String currency) {
            this.currency = currency;
            return this;
        }

        public PriceResponseDtoBuilder withBrandId(Long brandId) {
            this.brandId = brandId;
            return this;
        }

        public PriceResponseDtoBuilder withPriceId(Long priceId) {
            this.priceId = priceId;
            return this;
        }

        public PriceResponseDtoBuilder withStartDate(String startDate) {
            this.startDate = startDate;
            return this;
        }

        public PriceResponseDtoBuilder withEndDate(String endDate) {
            this.endDate = endDate;
            return this;
        }

        public PriceResponseDto build() {
            return new PriceResponseDto(productId, price, currency, brandId, priceId, startDate, endDate);
        }
    }
}
