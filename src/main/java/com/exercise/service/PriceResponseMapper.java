package com.exercise.service;

import com.exercise.dto.PriceResponseDto;
import com.exercise.model.Price;
import org.springframework.stereotype.Component;

@Component
public class PriceResponseMapper {

    PriceResponseDto toPriceResponseDto(Price price) {
        return PriceResponseDto.PriceResponseDtoBuilder.aPriceResponseDto()
                .withProductId(price.getProductId())
                .withBrandId(price.getBrandId())
                .withStartDate(price.getStartDate().toString())
                .withEndDate(price.getEndDate().toString())
                .withPriceId(price.getPriceId())
                .withPrice(price.getPrice())
                .withCurrency(price.getCurrency())
                .build();
    }

}
