package com.exercise.service;

import com.exercise.dto.PriceResponseDto;

import java.time.LocalDateTime;
import java.util.Optional;

public interface PriceService {

    Optional<PriceResponseDto> getProductPriceForDateTime(Long productId, Long brandId, LocalDateTime dateTime);

}