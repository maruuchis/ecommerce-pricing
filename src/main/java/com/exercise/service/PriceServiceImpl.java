package com.exercise.service;

import com.exercise.model.Price;
import com.exercise.dto.PriceResponseDto;
import com.exercise.repository.PriceRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class PriceServiceImpl implements PriceService {

    private final PriceRepository priceRepository;
    private final PriceResponseMapper priceResponseMapper;

    public PriceServiceImpl(PriceRepository priceRepository, PriceResponseMapper priceResponseMapper) {
        this.priceRepository = priceRepository;
        this.priceResponseMapper = priceResponseMapper;
    }

    public Optional<PriceResponseDto> getProductPriceForDateTime(Long productId, Long brandId, LocalDateTime dateTime) {

        List<Price> prices = priceRepository.findByProductIdAndBrandIdAndDateTime(productId, brandId, dateTime);

        Optional<Price> bestPriorityPrice = prices.stream().max(Comparator.comparingInt(Price::getPriority));

        return bestPriorityPrice.map(priceResponseMapper::toPriceResponseDto);
    }

}
