package com.exercise.exception;

import java.time.LocalDateTime;

public class PriceNotFoundException extends RuntimeException{

    public PriceNotFoundException(Long productId, Long brandId, LocalDateTime dateTime){
        super(String.format("Price not found for productId: %s, brandId: %s, dateTime: %s", productId, brandId, dateTime));
    }
}
