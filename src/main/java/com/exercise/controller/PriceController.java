package com.exercise.controller;

import com.exercise.dto.PriceResponseDto;
import com.exercise.exception.PriceNotFoundException;
import com.exercise.service.PriceService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Optional;

@RestController
public class PriceController {

    private final PriceService priceService;

    public PriceController(PriceService priceService) {
        this.priceService = priceService;
    }

    @GetMapping("/prices")
    public PriceResponseDto getProductPrice(
            @RequestParam Long productId,
            @RequestParam Long brandId,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateTime) {

        Optional<PriceResponseDto> productPrice = priceService.getProductPriceForDateTime(productId, brandId, dateTime);

        return productPrice
                .orElseThrow(() -> new PriceNotFoundException(productId, brandId, dateTime));
    }
}
